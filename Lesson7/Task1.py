"""
    Завдання 1
Напишіть функцію, яка змінює значення глобальної змінної.
    """

#global variable
CITY = "Kyiv"

# print value of global variable
print(f"I want to visit {CITY} next year!")


def travel_plans():
    """A dummy docstring."""
    global CITY
    CITY = "London"
    # print new value
    print(f"I want to visit {CITY} next year!")


travel_plans()

# print value of global variable
# print(f"I want to visit {CITY} next year!")
