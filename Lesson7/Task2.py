"""
    Завдання 2
Напишіть рекурсивну функцію, щоб порахувати значення факторіалу числа, яке передається
аргументом (див. рекомендовані ресурси).


Області видимості: https://pyneng.readthedocs.io/ru/latest/book/09_functions/2_namespace.html
Факторіал: https://uk.wikipedia.org/wiki/Факторіал
"""


def factorial(n):
    """func"""
    if n == 0:
        return 1  # умова виходу
    else:
        return n * factorial(n - 1)  # рекурсивний виклик


# обчислення факторіалу числа 5
print(factorial(8))
