# Завдання 1
# Вивести у консоль перші 100 чисел Фібоначчі (див. у рекомендованих ресурсах)
# start 0

# start = 0
# stop =99
# pre = start
# print(start)
# for i in range(start+1, stop+1):
#     print(i +pre)
#     pre = i + pre


# FROM SITE https://www.faceprep.in/python/fibonacci-series-in-python/
# Python program to generate Fibonacci series until 'n' value
n = int(input("Enter the value of 'n': "))
a = 0
b = 1
sum = 0
count = 1
print("Fibonacci Series: ", end=" ")
while(count <= n):
    print(sum, end=" ")
    count += 1
    a = b
    b = sum
    sum = a + b
print("Thanks")
