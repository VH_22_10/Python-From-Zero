# Завдання 3
# На вхід подається будь-яке число. Дізнатися, скільки розрядів у числі (1, 2, 3 або більше) та чи
# воно додатнє, або від’ємне. Наприклад, 25 - це двозначне додатнє, а -345 - це тризначне
# від’ємне, а 2400 - це 3-х і більш значне додатнє. 0 враховувати як окремий варіант, а не як
# однозначне число.

number = int(input("Введіть ціле число: "))

sing = "позитивне" if number > 0 else "негативне"

if number == 0:
    print("Варіант - 0")
elif len(str(abs(number))) == 1:
    print(f"Один знак і {sing}")
elif len(str(abs(number))) == 2:
    print(f"Двозначне і {sing}")
elif len(str(abs(number))) == 3:
    print(f"три знаки і {sing}")
else:
    print(f"Багатозначне і {sing}")
